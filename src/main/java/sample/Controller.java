package sample;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public ImageView slogan;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        File img = new File("src/main/java/resources/title.png");
        slogan.setImage(new Image(img.toURI().toString()));
    }

    public void onClick(ActionEvent actionEvent) throws IOException {
        System.out.println(FetchData.doFetch());
    }
}
